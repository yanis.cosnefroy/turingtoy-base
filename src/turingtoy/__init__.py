from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)


import poetry_version

__version__ = poetry_version.extract(source_file=__file__)


def run_turing_machine(
    machine: Dict,
    input_: str,
    steps: Optional[int] = None,
) -> Tuple[str, List, bool]:

    first_pos = 0
    char_blank = machine["blank"]
    history_end = machine["final states"]
    table_transition = machine["table"]
    str_to_char: list[str] = list(input_)
    liste = []
    current_state = machine["start state"]
    boolean = False

    while (current_state == history_end) == False:

        lenght_input = len(str_to_char)

        if steps == True:
            break

        if first_pos < 0:
            str_to_char.insert(0, char_blank)
            first_pos = 0

        elif first_pos >= lenght_input:
            str_to_char.append(char_blank)

        current_char = str_to_char[first_pos]
        if current_char not in table_transition[current_state]:
            break

        next_current_state = table_transition[current_state][current_char]

        char_to_str = convert_func(str_to_char)
        dictionary = {
            "state": current_state,
            "reading": current_char,
            "position": first_pos,
            "memory": char_to_str,
            "transition": next_current_state,
        }

        liste.append(dictionary)

        if next_current_state == "R":
            first_pos = first_pos + 1

        elif next_current_state == "L":
            first_pos = first_pos - 1

        else:
            if ("write" in next_current_state) == True:
                str_to_char[first_pos] = next_current_state["write"]

            if ("R" in next_current_state) == True:
                current_state = next_current_state["R"]
                first_pos = first_pos + 1

            elif ("L" in next_current_state) == True:
                current_state = next_current_state["L"]
                first_pos = first_pos - 1

    if current_state in history_end:
        boolean = True

    char_to_str = convert_func(str_to_char)
    res = strip_func(char_to_str)
    return (res, liste, boolean)


def convert_func(s):
    new = ""
    for x in s:
        new += x
    return new


def strip_func(s):
    start = 0
    end = len(s)

    while start < end and s[start].isspace():
        start = start + 1

    while end > start and s[end - 1].isspace():
        end = end - 1

    return s[start:end]
